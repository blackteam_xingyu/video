﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using video.Common;
using video.Model;
using System.ComponentModel;
using System.Windows.Media;
using System.Timers;
using System.IO;

namespace video.ViewModels
{

    public class DownLoadingViewModel : NotifyBase
    {
        #region 字段

        #endregion
        #region 属性
        public static ObservableCollection<DownLoadingModel> DownLoadingModels { get; set; } = new ObservableCollection<DownLoadingModel>();
        /// <summary>
        /// 声明开始按钮
        /// </summary>
        public CommandBase Start { get; set; } = new CommandBase();
        /// <summary>
        /// 声明暂停按钮
        /// </summary>
        public CommandBase Pause { get; set; } = new CommandBase();
        /// <summary>
        /// 声明取消按钮
        /// </summary>
        public CommandBase Cancel { get; set; } = new CommandBase();
        /// <summary>
        /// 声明删除按钮
        /// </summary>
        public CommandBase Delete { get; set; } = new CommandBase();
        /// <summary>
        /// 声明属性按钮
        /// </summary>
        public CommandBase Attribute { get; set; } = new CommandBase();
        #endregion
        #region 方法
        /// <summary>
        /// 多线程窗口处理方法
        /// </summary>
        /// <param name="msg"></param>


        /// <summary>
        /// 添加项目
        /// </summary>
        /// <param name="_Li">状态</param>
        /// <param name="_LiName">状态标签</param>
        /// <param name="_FileName">视频文件</param>
        /// <param name="_Speed">下载速度</param>
        /// <param name="_Schedule">下载进度</param>
        /// <param name="_Time">预计时间</param>
        public static void InitDownLoadingModels(string _URL, string _FileName, string _DownLoad_FlieAddress)
        {
            DownLoadingModels.Add(new DownLoadingModel { URL = _URL, FileName = _FileName, DownLoad_FlieAddress = _DownLoad_FlieAddress });
        }
        #endregion

        #region 构造器
        public DownLoadingViewModel()
        {

            #region 开始按钮事件
            this.Start.DoExcute = new Action<object>((o) =>
            {
                var i = (o as ContentPresenter).Content as DownLoadingModel;
                i.MyStyle = Application.Current.FindResource("下载中") as Style;
                i.StartDownLoad(i.DownLoad_FlieAddress + @"/" + i.FileName, i.URL);
                
            });
            this.Start.DoCanExecute = new Func<object, bool>((o) => { return true; });
            #endregion

            #region 暂停按钮事件
            this.Pause.DoExcute = new Action<object>((o) =>
            {
                var i = (o as ContentPresenter).Content as DownLoadingModel;
                i.PauseDownload();
                i.MyStyle = Application.Current.FindResource("已暂停") as Style;
            });
            this.Pause.DoCanExecute = new Func<object, bool>((o) => { return true; });
            #endregion

            #region 取消按钮事件
            this.Cancel.DoExcute = new Action<object>((o) =>
            {
                var i = (o as ContentPresenter).Content as DownLoadingModel;
                i.CancelDownload();
                i.MyStyle= Application.Current.FindResource("已取消") as Style;
            });
            this.Cancel.DoCanExecute = new Func<object, bool>((o) => { return true; });
            #endregion

            #region 删除按钮事件
            this.Delete.DoExcute = new Action<object>((o) =>
            {
                var i = (o as ContentPresenter).Content as DownLoadingModel;
                i.DeleteDownLoad();
            });
            this.Delete.DoCanExecute = new Func<object, bool>((o) => { return true; });
            #endregion
            #region 属性事件
            this.Attribute.DoExcute = new Action<object>((o) =>
            {
                var i = (o as ContentPresenter).Content as DownLoadingModel;
                MessageBox.Show("文件名：" + i.FileName + "\n" + "下载地址：" + i.URL + "\n" + "下载路径：" + i.DownLoad_FlieAddress);

            });
            this.Attribute.DoCanExecute = new Func<object, bool>((o) => { return true; });
            #endregion

        }
        #endregion
    }
}
