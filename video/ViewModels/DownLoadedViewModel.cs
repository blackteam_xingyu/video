﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using video.Common;
using video.Model;
using System.Diagnostics;
using System.Windows;

namespace video.ViewModels
{

    public class DownLoadedViewModel
    {

        #region 字段

        #endregion
        #region 属性
        public static ObservableCollection<DownLoadedModel> DownLoadedModels { get; set; } = new ObservableCollection<DownLoadedModel>();
        public CommandBase OpenFile { get; set; } = new CommandBase();
        public CommandBase OpenFlord { get; set; } = new CommandBase();
        public CommandBase ReDownLoad { get; set; } = new CommandBase();
        public CommandBase DeDoc { get; set; } = new CommandBase();
        public CommandBase Attribute { get; set; } = new CommandBase();
        #endregion
        #region 方法
        public static void InitDownLoadedModels(string _URL, string _FileName, string _FileAddress)
        {
            DownLoadedModels.Add(new DownLoadedModel { URL = _URL, FileName = _FileName, FileAddress = _FileAddress });
        }
        #endregion
        #region 构造器
        public DownLoadedViewModel()
        {
            this.OpenFile.DoExcute = new Action<object>((o) =>
            {
                var i = (o as ContentPresenter).Content as DownLoadedModel;
                try
                {
                    Process p = new Process();
                    p.StartInfo.FileName = i.FileAddress + @"\" + i.FileName;
                    p.Start();
                }
                catch (Exception)
                {
                    MessageBox.Show("无法找到文件，请检查文件是否删除", "错误", default, MessageBoxImage.Error);
                    throw;
                }

                
            });  
            this.OpenFile.DoCanExecute = new Func<object, bool>((o) => { return true; });
            this.OpenFlord.DoExcute = new Action<object>((o) =>
            {
                var i = (o as ContentPresenter).Content as DownLoadedModel;
                Process p = new Process();
                p.StartInfo.FileName = i.FileAddress;
                p.Start();

            });
            this.OpenFlord.DoCanExecute = new Func<object, bool>((o) => { return true; });
            this.ReDownLoad.DoExcute = new Action<object>((o) =>
            {
                var i = (o as ContentPresenter).Content as DownLoadedModel;
                if (MessageBox.Show("是否重新下载视频？", "警告", MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.OK)
                {
                    DownLoadingViewModel.InitDownLoadingModels(i.URL, i.FileName, i.FileAddress);
                    DownLoadedModels.Remove(i);
                }

            });
            this.ReDownLoad.DoCanExecute = new Func<object, bool>((o) => { return true; });
            this.DeDoc.DoExcute = new Action<object>((o) =>
            {
                var i = (o as ContentPresenter).Content as DownLoadedModel;
                var msg = MessageBox.Show("是否同时删除文件？", "警告", MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.No);
                if (msg == MessageBoxResult.Yes)
                {
                    try
                    {
                        File.Delete(i.FileAddress+@"\"+i.FileName);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("删除文件失败！", "错误", MessageBoxButton.OK,MessageBoxImage.Error);
                        throw;
                    }
                    RecycleBinViewModel.InitRecycleBinModels(i.URL, i.FileName, i.FileAddress);
                    DownLoadedModels.Remove(i);
                }
                else if (msg == MessageBoxResult.No)
                {
                    RecycleBinViewModel.InitRecycleBinModels(i.URL, i.FileName, i.FileAddress);
                    DownLoadedModels.Remove(i);
                }
            });
            this.DeDoc.DoCanExecute = new Func<object, bool>((o) => { return true; });
            this.Attribute.DoExcute = new Action<object>((o) =>
            {
                var i = (o as ContentPresenter).Content as DownLoadedModel;
                MessageBox.Show("文件名："+i.FileName+"\n"+"下载地址："+i.URL+"\n"+"下载路径："+i.FileAddress);

            });
            this.Attribute.DoCanExecute = new Func<object, bool>((o) => { return true; });
        }
        #endregion
    }
}
