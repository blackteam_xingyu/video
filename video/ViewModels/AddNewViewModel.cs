﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using video.Common;
using video.Model;
using video.View;
using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Chrome;
using System.Threading;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace video.ViewModels
{

    public class AddNewViewModel : NotifyBase
    {
        #region 字段
        public string[] videoClasses = {".FLV",".flv",".F4V",".f4v",".MP4",".mp4",".mpeg",".MPEG", ".MPG",".mpg",".DAT",".dat",
            ".AVI",".avi",".nAVI",".navi",".MOV",".mov",".ASF",".asf",".WMV",".wmv",".NAVI",".navi",".3GP",".3gp",".RM",".rm",".RMVB",".rmvb"};
        public string personURL;
        public string personResult;
        public string personName;
        public IList<string> video_URL;

        #endregion

        #region 属性
        public static AddNewModel AddNewModel { get; set; } = new AddNewModel();

        public CommandBase CloseWindowCommand { get; set; } = new CommandBase();
        public CommandBase SaveCloseWindowCommand { get; set; } = new CommandBase();
        public CommandBase SearchButton { get; set; } = new CommandBase();
        public ObservableCollection<FileListModel> FileList { get; set; } = new ObservableCollection<FileListModel>();
        public CommandBase SelectAll_OnClick { get; set; } = new CommandBase();

        #endregion

        #region 方法
        /// <summary>
        /// 使用ChromeDriver加载网页并且获得视频标签(动态方法)
        /// </summary>
        /// <param name="PageURL">网页URL</param>
        /// <returns>视频下载地址的字符串集合</returns>
        private  IList<string> GetVideoURL(string PageURL = "")
        {
            var chromedrive = @"..\..\Resource\Driver\" + Page_Setting.WebDriveVersion;
            var chromeOptions = new ChromeOptions();
            ChromeDriverService driverService = ChromeDriverService.CreateDefaultService(chromedrive);
            driverService.HideCommandPromptWindow = true;//关闭cmd窗口
            chromeOptions.AddArguments("--test-type", "--ignore-certificate-errors");
            chromeOptions.AddArgument("enable-automation");
            chromeOptions.AddArguments("headless");
            try
            {
                using ( IWebDriver webDriver = new ChromeDriver(driverService, chromeOptions))
                {
                    webDriver.Navigate().GoToUrl(PageURL);
                    var response = webDriver.PageSource;
                    CloseChromeDriver(webDriver);
                    var document = new HtmlAgilityPack.HtmlDocument();
                    document.LoadHtml(response);
                    var videos = document.DocumentNode.SelectNodes("//video/@src");
                    IList<string> videoURLlist = new List<string>();
                    if (videos != null)
                    {
                        {
                            foreach (var video in videos)
                            {
                                string videoURL = video.Attributes["src"].Value;
                                videoURLlist.Add(videoURL);

                            }
                            return videoURLlist;

                        }
                    }

                    else
                    {
                        MessageBox.Show("未查找到视频标签，请确定URL是否正确");
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("WebDriver无法启动请确认安装了符合版本的Chrome浏览器");
                return null;
                throw;
            }

        }

        #region 异常  退出chromedriver

        [DllImport("user32.dll", EntryPoint = "FindWindow")]
        private extern static IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", EntryPoint = "SendMessage")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        public const int SW_HIDE = 0;
        public const int SW_SHOW = 5;

        [DllImport("user32.dll", EntryPoint = "ShowWindow")]
        public static extern int ShowWindow(IntPtr hwnd, int nCmdShow);

        /// <summary>
        /// 获取窗口句柄
        /// </summary>
        /// <returns></returns>
        public IntPtr GetWindowHandle()
        {
            string name = (Environment.CurrentDirectory + "\\chromedriver.exe");
            IntPtr hwd = FindWindow(null, name);
            return hwd;
        }

        /// <summary>
        /// 关闭chromedriver窗口
        /// </summary>
        public void CloseWindow()
        {
            try
            {
                IntPtr hwd = GetWindowHandle();
                SendMessage(hwd, 0x10, 0, 0);
            }
            catch { }
        }

        /// <summary>
        /// 退出chromedriver
        /// </summary>
        /// <param name="driver"></param>
        public void CloseChromeDriver(IWebDriver driver)
        {
            try
            {
                driver.Quit();
                driver.Dispose();
            }
            catch { }
            CloseWindow();
        }

        #endregion 异常  退出chromedriver

        /// <summary>
        /// 通过URL获取HTML并截取视频文件地址(静态方法)
        /// </summary>
        //private async Task<IList<string>> GetVideoURL(string PageURL = "")
        //{
        //    var client = new HttpClient();
        //    var response = await client.GetStringAsync(PageURL);
        //    var document = new HtmlAgilityPack.HtmlDocument();
        //    document.LoadHtml(response);
        //    var videos = document.DocumentNode.SelectNodes("//video");
        //    IList<string> videoURLlist = new List<string>();
        //    MessageBox.Show(response,"HTML");
        //    if (videos == null)
        //    {
        //        MessageBox.Show("未查找到视频标签，请确定URL是否正确");
        //    }
        //    else
        //    {
        //        foreach (var video in videos)
        //        {
        //            var videoURL = video.Attributes["scr"].Value;
        //            videoURLlist.Add(videoURL);
        //        }
        //    }
        //    return videoURLlist;
        //}


        ///<summary>
        ///添加列表元素
        ///</summary>
        ///<param name="_FileName">文件名</param>
        ///<param name="_FileClass">文件类别</param>
        ///<param name="_IsCheckedOn">是否勾选CheckBox</param>
        private void InitFileList(string _FileURL, string _FileName, string _FileClass, bool _IsCheckedOn = false)
        {
            FileList.Add(new FileListModel { FileURL = _FileURL, FileName = _FileName, FileClass = _FileClass, IsCheckedOn = _IsCheckedOn });
        }
        #endregion

        #region 构造器
        public AddNewViewModel()
        {
            AddNewModel.FileSearch = @"C:\ProgramData\XingLingVideo\Downloadfiles";
            #region 关闭窗口事件

            this.CloseWindowCommand.DoExcute = new Action<object>((o) =>
            {
                try
                {
                    video_URL = null;
                    (o as Window).Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("按键异常！窗口无法关闭！", "警告！", MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);
                    throw;
                }

            }
            );
            this.CloseWindowCommand.DoCanExecute = new Func<object, bool>((o) => { return true; });
            #endregion
            #region 窗口保存关闭事件
            this.SaveCloseWindowCommand.DoExcute = new Action<object>((o) =>
            {
                try
                {
                    foreach (var item in FileList)
                    {
                        if (item.IsCheckedOn == true)
                        {
                            DownLoadingViewModel.InitDownLoadingModels(item.FileURL, item.FileName, AddNewModel.FileSearch);
                        }
                    }
                    (o as Window).Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("内存异常！窗口无法关闭！无法创建下载", "警告！", MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);
                    throw;
                }

            }
);
            this.SaveCloseWindowCommand.DoCanExecute = new Func<object, bool>((o) => { return true; });
            #endregion
            #region 点击搜索键事件
            this.SearchButton.DoExcute = new Action<object>(async (o) =>
            {
                video_URL = await Task.Run(() => GetVideoURL(AddNewModel._URL));
                try
                {
                    foreach (string person in video_URL)
                    {
                        foreach (string videoClass in videoClasses)
                        {
                            int persons = person.IndexOf(videoClass);

                            if (persons < 0)
                            {
                                personResult = "未知文件类型";
                                personName = person;
                                personURL = person;

                            }
                            else
                            {
                                int personstart = person.LastIndexOf("/", persons);
                                personURL = person;
                                personResult = videoClass;
                                personName = person.Substring(personstart + 1, persons + videoClass.Length - personstart - 1);
                                break;
                            }

                        }
                        this.InitFileList(personURL, personName, personResult);
                        personURL = null;
                        personResult = null;
                        personName = null;
                    }
                    video_URL = null;
                }
                catch (Exception)
                {
                    MessageBox.Show("未查找到视频文件地址，请确认URL是否正确");
                }
            }
            );
            this.SearchButton.DoCanExecute = new Func<object, bool>((o) => { return true; });
            #endregion
            AddNewModel._URL = "";
            AddNewModel.FileSearch = Page_Setting.Defaultfile;
        }
        #endregion


    }
}
