﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using video.Common;
using video.View;
namespace video.ViewModels
{

    public class MainViewModel:NotifyBase
    {

        #region 字段

        #endregion

        #region 属性
        public CommandBase CloseWindowCommand { get; set; }
        public Page_Download page_download = new Page_Download();
        private FrameworkElement _mainContent;

        public FrameworkElement MainContent
        {
            get { return _mainContent; }
            set { _mainContent = value; this.DoNotify(); }
        }
        public CommandBase NavChangedCommand { get; set; }
        #endregion

        #region 方法
        private void DoNavChanged(object obj)
        {
            Type type = Type.GetType("video.View." + obj.ToString());
            ConstructorInfo cti = type.GetConstructor(System.Type.EmptyTypes);
            this.MainContent = (FrameworkElement)cti.Invoke(null);
        }

        #endregion

        #region 构造器
        public MainViewModel()
        {
            #region 关闭窗口事件
            this.CloseWindowCommand = new CommandBase();
            this.CloseWindowCommand.DoExcute = new Action<object>((o) =>
                  {
                      (o as Window).Close();
                  }
            );
            this.CloseWindowCommand.DoCanExecute = new Func<object, bool>((o) => { return true; });
            #endregion
            this.MainContent = page_download;
            this.NavChangedCommand = new CommandBase();
            this.NavChangedCommand.DoExcute = new Action<object>(DoNavChanged);
            this.NavChangedCommand.DoCanExecute = new Func<object, bool>((o)=>true);
            
        }
        #endregion
    }
}
