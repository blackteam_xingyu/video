﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using video.Common;
using video.View;


namespace video.ViewModels
{

    public class Page_DownloadViewModel : NotifyBase

    {

        #region 字段
        public static DownLoading downLoading = new DownLoading();
        #endregion

        #region 属性

        private FrameworkElement downLoadContent;

        public FrameworkElement DownLoadContent
        {
            get { return downLoadContent; }
            set { downLoadContent = value; this.DoNotify(); }
        }
        public CommandBase AllStart { get; set; } = new CommandBase();
        public CommandBase CloseWindowCommand { get; set; } = new CommandBase();
        public CommandBase AllCancel { get; set; } = new CommandBase();
        public CommandBase NavChangedCommand { get; set; } = new CommandBase();
        public CommandBase ShutDown { get; set; } = new CommandBase();
        #endregion

        #region 方法
        private void DoNavChanged(object obj)
        {
            Type type = Type.GetType("video.View." + obj.ToString());
            ConstructorInfo cti = type.GetConstructor(System.Type.EmptyTypes);
            this.DownLoadContent = (FrameworkElement)cti.Invoke(null);
        }
        #endregion

        #region 构造器
        public Page_DownloadViewModel()
        {
            #region 关闭窗口事件触发
            
            this.CloseWindowCommand.DoExcute = new Action<object>((o) =>
            {
                Environment.Exit(0);
            }
            );
            this.CloseWindowCommand.DoCanExecute = new Func<object, bool>((o) => { return true; });
            #endregion
            this.ShutDown.DoExcute = new Action<object>((o) =>
            {
                if (MessageBox.Show("将在所有任务完成后关机，此功能不够完善暂时不可逆", "警告", MessageBoxButton.OKCancel, MessageBoxImage.Warning, MessageBoxResult.Cancel) == MessageBoxResult.OK)
                {
                    Timer timer = new Timer(30000);
                    timer.Elapsed += new ElapsedEventHandler(Timershutdown);
                    timer.AutoReset = true;
                    timer.Enabled = true;

                    void Timershutdown(object source, ElapsedEventArgs e)
                    {
                        if (Foreacher())
                        {
                            shutdown();
                        }
                    }
                    void shutdown()
                    {
                        Process myProcess = new Process(); myProcess.StartInfo.FileName = "cmd.exe";
                        myProcess.StartInfo.UseShellExecute = false;
                        myProcess.StartInfo.RedirectStandardInput = true;
                        myProcess.StartInfo.RedirectStandardOutput = true;
                        myProcess.StartInfo.RedirectStandardError = true;
                        myProcess.StartInfo.CreateNoWindow = true; myProcess.Start();
                        myProcess.StandardInput.WriteLine("shutdown -s -t 0");
                    }
                    bool Foreacher()
                    {
                        bool i = true;
                        foreach (var item in DownLoadingViewModel.DownLoadingModels)
                        {
                            if (item.IsCompleted == false)
                            {
                                i = false;
                                break;
                            }

                        }
                        return i;
                    }
                }
            }
            );
            this.ShutDown.DoCanExecute = new Func<object, bool>((o) => { return true; });
            this.AllCancel.DoExcute = new Action<object>(async(o) =>
            {
                if (MessageBox.Show("是否取消所有正在下载的任务？","询问",MessageBoxButton.OKCancel,MessageBoxImage.Question,MessageBoxResult.Cancel)==MessageBoxResult.OK)
                {
                    await Task.Run(() =>
                    {
                        foreach (var item in DownLoadingViewModel.DownLoadingModels)
                        {
                            item.CancelDownload();
                        }
                    });
                }
            }
            );
            this.AllCancel.DoCanExecute = new Func<object, bool>((o) => { return true; });
            this.AllStart.DoExcute = new Action<object>(async (o) =>
            {
                if (MessageBox.Show("是否开始所有正在下载的任务？", "询问", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel) == MessageBoxResult.OK)
                {
                    await Task.Run(() =>
                    {
                        foreach (var item in DownLoadingViewModel.DownLoadingModels)
                        {
                            item.StartDownLoad(item.FileName,item.URL);
                        }
                    });
                }
            }
            );
            this.AllStart.DoCanExecute = new Func<object, bool>((o) => { return true; });
            this.DownLoadContent = downLoading;
            this.NavChangedCommand.DoExcute = new Action<object>(DoNavChanged);
            this.NavChangedCommand.DoCanExecute = new Func<object, bool>((o) => true);
        }
        #endregion


    }
}
