﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using video.Common;
using video.Model;

namespace video.ViewModels
{

    public class RecycleBinViewModel
    {

        #region 字段

        #endregion
        #region 属性
        public static ObservableCollection<RecycleBinModel> RecycleBinModels { get; set; } = new ObservableCollection<RecycleBinModel>();
        public CommandBase ReDownLoad { get; set; } = new CommandBase();
        public CommandBase DeDoc { get; set; } = new CommandBase();
        /// <summary>
        /// 声明属性按钮
        /// </summary>
        public CommandBase Attribute { get; set; } = new CommandBase();
        #endregion
        #region 方法
        public static void InitRecycleBinModels(string _URL, string _FileName, string _FileAddress)
        {
            RecycleBinModels.Add(new RecycleBinModel { URL = _URL, FileName = _FileName, FileAddress = _FileAddress });
        }
        #endregion
        #region 构造器
        public RecycleBinViewModel()
        {
            this.ReDownLoad.DoExcute = new Action<object>((o) =>
            {
                var i = (o as ContentPresenter).Content as RecycleBinModel;
                if (MessageBox.Show("是否重新下载此视频？","询问",MessageBoxButton.YesNo,MessageBoxImage.Question)==MessageBoxResult.Yes)
                {
                    DownLoadingViewModel.InitDownLoadingModels(i.URL, i.FileName, i.FileAddress);
                    RecycleBinModels.Remove(i);
                }
            });
            this.ReDownLoad.DoCanExecute = new Func<object, bool>((o) => { return true; });
            this.DeDoc.DoExcute = new Action<object>((o) =>
            {
                var i = (o as ContentPresenter).Content as RecycleBinModel;
                RecycleBinModels.Remove(i);
            });
            this.DeDoc.DoCanExecute = new Func<object, bool>((o) => { return true; });
            #region 属性事件
            this.Attribute.DoExcute = new Action<object>((o) =>
            {
                var i = (o as ContentPresenter).Content as RecycleBinModel;
                MessageBox.Show("文件名：" + i.FileName + "\n" + "下载地址：" + i.URL + "\n" + "下载路径：" + i.FileAddress);
            });
            this.Attribute.DoCanExecute = new Func<object, bool>((o) => { return true; });
            #endregion
        }
        #endregion
    }
}
