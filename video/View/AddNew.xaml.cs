﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using video.ViewModels;
using video.Model;

namespace video.View
{
    /// <summary> 
    /// AddNew.xaml 的交互逻辑
    /// </summary>
    public partial class AddNew : Window
    {
        #region 构造器
        public AddNew()
        {
            InitializeComponent();           
            this.DataContext = new AddNewViewModel();
        }
        #endregion

        #region 方法
        private void Move_MouseMove(object sender, MouseEventArgs e)

        {

            if (e.LeftButton == MouseButtonState.Pressed)

            {

                this.DragMove();

            }

        }
        #endregion
    }
}
