﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using video.Model;

namespace video.View
{
    /// <summary>
    /// Setting.xaml 的交互逻辑
    /// </summary>
    public partial class Page_Setting : UserControl
    {
        #region 构造器
        public Page_Setting()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #endregion

        #region 字段
        public static string Defaultfile;
        
        public static string WebDriveVersion = "90";
        string webDriveVersion = "90" ;
        string[] _webDriveVersion = new string[] {"91","90","89","88","87","86","85","84","83","81","80","79","78","77","76","75","74",
                                                           "71-73","68-70","65-67","62-64","59-61"};
        #endregion
        #region 属性
        public static Page_SettingModel Page_SettingModel { get; set; } = new Page_SettingModel();
        
        #endregion
        #region 方法
        private void combobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string SelectedTag = (this.combobox.SelectedItem as ComboBoxItem).Tag.ToString();
            webDriveVersion = _webDriveVersion[int.Parse(SelectedTag)];
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("是否保存", "保存", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes) 
            {
                WebDriveVersion = webDriveVersion;
                Defaultfile = Page_SettingModel.defaultfile;
            }
            else{}
        }

        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("是否重置", "重置", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                WebDriveVersion = "90";
                webDriveVersion = "90";
                Defaultfile = null;
                Page_SettingModel.defaultfile = null;
            }
            else{}

        }
        #endregion
    }
}
