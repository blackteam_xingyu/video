﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using video.ViewModels;

namespace video.View
{
    /// <summary>
    /// Page_Download.xaml 的交互逻辑
    /// </summary>
    public partial class Page_Download : UserControl
    {
        #region 构造器


        public Page_Download()
        {
            InitializeComponent();
            this.DataContext = new Page_DownloadViewModel();
        }
        #endregion

        #region 方法      
        private void 新建_Click(object sender, RoutedEventArgs e)
        {
            AddNew addNew = new AddNew();
            addNew.ShowDialog();
        }
        private void setting_left_Click(object sender, RoutedEventArgs e)
        {
            this.setting_menu.PlacementTarget = this.setting_left;
            this.setting_menu.IsOpen = true;

        }
        #endregion

    }
}
