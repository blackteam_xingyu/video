﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using video.View;
using video.ViewModels;
namespace video
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    [System.Runtime.InteropServices.Guid("B67012EB-74D8-4D08-B6D3-7E7454FA7487")]
    public partial class MainWindow : Window
    {
        #region 构造器
        public MainWindow()
        {
            InitializeComponent();

            this.DataContext = new MainViewModel();


        }
        #endregion

        #region 字段
        public static Rect rcnormal;
        #endregion

        #region 方法
        private void Move_MouseMove(object sender, MouseEventArgs e)

        {

            if (e.LeftButton == MouseButtonState.Pressed)

            {

                this.DragMove();

            }

        }



        #region 最大化事件
        private void ToggleMaximum_Click(object sender, RoutedEventArgs e)
        {
            this.复原.Visibility = Visibility.Visible;//使还原图标显示

            this.最大化.Visibility = Visibility.Collapsed;//使最大化图标隐藏

            rcnormal = new Rect(this.Left, this.Top, this.Width, this.Height);//保存下当前位置与大小

            Rect rc = SystemParameters.WorkArea;//获取工作区大小


            this.BorderThickness = new Thickness(0, 0, 0, 0);//隐藏阴影




            this.Left = 0;//设置位置


            this.Top = 0;


            this.Width = rc.Width;


            this.Height = rc.Height;
        }
        #endregion

        #region 复原事件
        private void ToggleReturn_Click(object sender, RoutedEventArgs e)
        {
            this.BorderThickness = new Thickness(5);

            this.Left = rcnormal.Left;


            this.Top = rcnormal.Top;


            this.Width = rcnormal.Width;


            this.Height = rcnormal.Height;



            this.最大化.Visibility = Visibility.Visible;


            this.复原.Visibility = Visibility.Collapsed;
        }
        #endregion

        #region 最小化事件
        private void ToggleMinimum_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }
        #endregion
        #endregion

    }

}
