﻿using System;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Xps.Packaging;
using System.IO;
using System.Windows.Documents;
using System.Windows;

namespace video.View
{
    /// <summary>
    /// Page_About.xaml 的交互逻辑
    /// </summary>
    public partial class Page_About : UserControl
    {

        public Page_About()
        {
            InitializeComponent();
            XpsDocument document = new XpsDocument("../../Resource/About_Document.xps", System.IO.FileAccess.Read);
            about_document.Document = document.GetFixedDocumentSequence();
            about_document.FitToWidth();
        }
    }
}
