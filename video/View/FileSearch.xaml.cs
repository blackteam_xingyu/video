﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using video.ViewModels;

namespace video.View
{
    /// <summary>
    /// FileSearch.xaml 的交互逻辑
    /// </summary>
    public partial class FileSearch : UserControl
    {
        #region 字段
        public System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();

        public static readonly DependencyProperty textProperty
            = DependencyProperty.Register("TextProperty", typeof(string), typeof(FileSearch),null);

        #endregion

        #region 属性


        public  string TextProperty
        {
            get { return (string)GetValue(textProperty); }
            set { SetValue(textProperty, value); }
        }

        #endregion

        #region 方法

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            fbd.Description = "请选择一个文件或文件夹";
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                TextProperty = fbd.SelectedPath;
            }
        }
        #endregion

        #region 构造器
        public FileSearch()
        {
            InitializeComponent();
        }
        #endregion



      
    }
}
