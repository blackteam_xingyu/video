﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using video.Common;

namespace video.Model
{

    public  class FileListModel:NotifyBase
    {
        public string FileURL { get; set; }
        public string FileName { get; set; }

        public string FileClass { get; set; }

        private bool _isCheckedOn = false;

        public bool IsCheckedOn
        {
            get { return _isCheckedOn; }
            set { _isCheckedOn = value;this.DoNotify(); }
        }

    }
}
