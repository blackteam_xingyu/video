﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using System.Windows;
using video.Common;
using video.ViewModels;

namespace video.Model
{
    public class DownLoadingModel : NotifyBase
    {
        #region 字段
        public int readBytes = 100 * 1024;
        public HttpWebRequest request;
        /// <summary>
        /// 多线程同步的字段
        /// </summary>
        static readonly object syncObject = new object();

        #endregion
        #region 属性
        private bool isCompleted;

        public bool IsCompleted
        {
            get { return isCompleted; }
            set { isCompleted = value; this.DoNotify(); }
        }

        private bool isDownloading;

        public bool IsDownloading
        {
            get { return isDownloading; }
            set { isDownloading = value; this.DoNotify(); }
        }
        private Style myStyle;
        /// <summary>
        /// 状态栏图标样式
        /// </summary>
        public Style MyStyle
        {
            get { return myStyle; }
            set { myStyle = value; this.DoNotify(); }
        }
        /// <summary>
        /// 文件地址
        /// </summary>
        public string URL { get; set; }
        /// <summary>
        /// 下载文件名
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// 下载地址
        /// </summary>
        public string DownLoad_FlieAddress { get; set; }
        private string speed;
        /// <summary>
        /// 下载速度
        /// </summary>
        public string Speed
        {
            get { return speed; }
            set { speed = value; this.DoNotify(); }
        }
        private double realSpeed;

        public double RealSpeed
        {
            get { return realSpeed; }
            set { realSpeed = value; this.DoNotify(); }
        }

        private double schedule;
        /// <summary>
        /// 下载进度实现
        /// </summary>
        public double Schedule
        {
            get { return schedule; }
            set { schedule = value; this.DoNotify(); }
        }
        private string scheduleText;
        public string ScheduleText
        {
            get { return scheduleText; }
            set { scheduleText = value; this.DoNotify(); }
        }

        private long scheduleByte;
        /// <summary>
        /// 已接收的字节数
        /// </summary>
        public long ScheduleByte
        {
            get { return scheduleByte; }
            set { scheduleByte = value; this.DoNotify("ScheduleByte", "Schedule"); }
        }
        private long totalschedule;

        public long Totalschedule
        {
            get { return totalschedule; }
            set { totalschedule = value; this.DoNotify("Totalschedule", "Schedule"); }
        }
        private long oldScheduleByte;
        /// <summary>
        /// 1s前字节数
        /// </summary>
        public long OldScheduleByte
        {
            get { return oldScheduleByte; }
            set { oldScheduleByte = value; }
        }

        private string time;
        /// <summary>
        /// 预计时间
        /// </summary>
        public string Time
        {
            get { return time; }
            set { time = value; this.DoNotify(); }
        }
        #endregion

        #region 方法
        /// <summary>
        /// 
        /// </summary>
        /// <param name="myTime">格式必须为000000</param>
        /// <param name="HMS">默认为"HH:MM:SS"</param>
        /// <returns></returns>

        /// <summary>
        /// 文件转化为流
        /// </summary>
        /// <param name="fileName">文件名</param>
        /// <returns></returns>
        public byte[] FileToByte(string fileName)

        {

            // 打开文件

            FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

            // 读取文件的 byte[]

            byte[] bytes = new byte[fileStream.Length];

            fileStream.Read(bytes, 0, bytes.Length);

            fileStream.Close();
            return bytes;
        }
        public void ByteToFile(byte[] bytes, string fileName)
        {
            FileStream fileStream = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Write);
            fileStream.Write(bytes, 0, bytes.Length);
            fileStream.Close();
        }
        #endregion
        public void StartDownLoad(string _FileName, string _URl)
        {


            request = (HttpWebRequest)FileWebRequest.Create(_URl);
            if (this.ScheduleByte > 0)
            {
                request.AddRange(this.ScheduleByte);
            }

            request.BeginGetResponse(ar =>
            {
                this.IsDownloading = true;
                var response = request.EndGetResponse(ar);
                if (this.Totalschedule == 0) this.Totalschedule = response.ContentLength;
                using (var writer = new FileStream(_FileName, FileMode.OpenOrCreate))
                {
                    using (var stream = response.GetResponseStream())
                    {
                        Timer timer = new Timer(1000);
                        timer.Elapsed += new ElapsedEventHandler(GetSpeed);
                        timer.AutoReset = true;
                        timer.Enabled = true;
                        while (this.IsDownloading)
                        {
                            byte[] data = new byte[this.readBytes];
                            int readNumber = stream.Read(data, 0, data.Length);

                            if (readNumber > 0)
                            {
                                writer.Write(data, 0, readNumber);
                                this.ScheduleByte += readNumber;
                                this.Schedule = ScheduleByte * 100 / Totalschedule;
                                this.ScheduleText = string.Format("正在下载视频，完成进度{0}%\n{1}/{2}(MB)", Schedule,
                                    Math.Round((double)(this.ScheduleByte) / 1048576, 2, MidpointRounding.AwayFromZero),
                                    Math.Round((double)(this.Totalschedule) / 1048576, 2, MidpointRounding.AwayFromZero)
                                    );
                            }
                            if (this.ScheduleByte == this.Totalschedule)
                            {
                                this.Complete();
                            }
                        }
                    }
                }
            }, null);


        }
        public void PauseDownload()
        {
            IsDownloading = false;
            this.Speed = null;
        }
        public void CancelDownload()
        {
            if (MessageBox.Show("请确认是否取消下载", "警告", MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.OK)
            {
                this.IsCompleted = false;
                this.IsDownloading = false;
                this.Speed = null;
                this.Schedule = 0;
                this.ScheduleByte = 0;
                this.Totalschedule = 0;
                this.request = null;
                try
                {
                    File.Delete(this.DownLoad_FlieAddress + @"\" + this.FileName);
                }
                catch (Exception)
                {
                    MessageBox.Show("删除已下载的损坏文件失败,可能文件被占用!", "错误", default, MessageBoxImage.Error);
                    throw;
                }

            }
        }

        public void DeleteDownLoad()
        {
            if (MessageBox.Show("请确认是否要删除", "警告", MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.OK)
            {
                this.IsCompleted = false;
                this.IsDownloading = false;
                this.Speed = null;
                this.Schedule = 0;
                this.ScheduleByte = 0;
                this.Totalschedule = 0;
                this.request = null;
                RecycleBinViewModel.InitRecycleBinModels(this.URL, this.FileName, this.DownLoad_FlieAddress);
                try
                {
                    File.Delete(this.DownLoad_FlieAddress + @"\" + this.FileName);
                }
                catch (Exception)
                {
                    MessageBox.Show("删除已下载的文件失败,可能文件被占用!", "错误", default, MessageBoxImage.Error);
                    throw;
                }
                System.Threading.ThreadPool.QueueUserWorkItem(delegate
                {
                    System.Threading.SynchronizationContext.SetSynchronizationContext(new
                        System.Windows.Threading.DispatcherSynchronizationContext(System.Windows.Application.Current.Dispatcher));
                    System.Threading.SynchronizationContext.Current.Post(pl =>
                    {
                        DownLoadingViewModel.DownLoadingModels.Remove(this);
                    }, null);
                });

            }

        }
        public void Complete()
        {
            this.IsCompleted = true;
            this.IsDownloading = false;
            this.Speed = null;

            System.Threading.ThreadPool.QueueUserWorkItem(delegate
            {
                System.Threading.SynchronizationContext.SetSynchronizationContext(new
                    System.Windows.Threading.DispatcherSynchronizationContext(System.Windows.Application.Current.Dispatcher));
                System.Threading.SynchronizationContext.Current.Post(pl =>
                {
                    DownLoadedViewModel.InitDownLoadedModels(this.URL, this.FileName, this.DownLoad_FlieAddress);
                    DownLoadingViewModel.DownLoadingModels.Remove(this);
                }, null);
            });

        }
        void GetSpeed(object source, ElapsedEventArgs e)
        {
            lock (syncObject)
            {
                this.RealSpeed = Math.Round(((double)this.ScheduleByte / 1048576) - ((double)this.OldScheduleByte / 1048576), 2, MidpointRounding.AwayFromZero);
                this.Speed = Convert.ToString(RealSpeed) + " MB/S";
                long iTime;
                try
                {
                    iTime = (Totalschedule - ScheduleByte) / (ScheduleByte - OldScheduleByte);
                }
                catch (Exception)
                {
                    iTime = 0;
                    throw;
                }
                long htime = iTime / 3600;
                long mtime = iTime % 3600 / 60;
                long stime = iTime % 3600 % 60;
                this.Time = string.Format("{0}:{1}:{2}", htime.ToString("00"), mtime.ToString("00"), stime.ToString("00"));
                this.OldScheduleByte = this.ScheduleByte;
            }

        }
        #region 构造器
        public DownLoadingModel()
        {
            IsDownloading = false;
            this.Time = "00:00:00";
            this.MyStyle = Application.Current.FindResource("未开始") as Style;
            this.OldScheduleByte = 0;

        }
        #endregion
    }
}
