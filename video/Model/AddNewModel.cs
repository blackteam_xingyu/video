﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using video.Common;

namespace video.Model
{

    public class AddNewModel: NotifyBase
    {
        private string _urls;

        public string _URL
        {
            get { return _urls; }
            set { _urls = value; this.DoNotify(); }
        }

        private string _filesearch;

        public string FileSearch
        {
            get { return _filesearch; }
            set { _filesearch = value; this.DoNotify(); }
        }
    }
}
