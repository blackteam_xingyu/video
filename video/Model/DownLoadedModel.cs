﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using video.Common;

namespace video.Model
{

    public class DownLoadedModel:NotifyBase
    {

        #region 字段

        #endregion
        #region 属性
        public string FileName { get; set; }
        public string FileAddress { get; set; }
        public string URL { get; set; }

        #endregion
        #region 方法

        #endregion
        #region 构造器

        #endregion
    }
}
