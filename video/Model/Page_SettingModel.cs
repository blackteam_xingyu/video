﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using video.Common;

namespace video.Model
{

    public class Page_SettingModel : NotifyBase
    {
        private string _defaultfile = @"C:\ProgramData\XingLingVideo\Downloadfiles";

        public string defaultfile
        {
            get { return _defaultfile; }
            set { _defaultfile = value; this.DoNotify(); }
        }
    }
}
